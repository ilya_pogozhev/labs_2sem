import numpy as np
import matplotlib.pyplot as plt

# discrete step
n1 = 1 / 1024
n2 = 1 / 16

# time
t1 = np.arange(0, 1, n1)
t2 = np.arange(0, 1, n2)

# discrete function
y1 = 2 * np.cos(2 * np.pi * 5 * t1) + np.cos(2 * np.pi * t1) + np.sin(2 * np.pi * 10 * t1)
y2 = 2 * np.cos(2 * np.pi * 5 * t2) + np.cos(2 * np.pi * t2) + np.sin(2 * np.pi * 10 * t2)

# graph
plt.suptitle("Шаг дискретизации")
plt.plot(t1, y1, 'deeppink')
plt.step(t2, y2, 'lime')
plt.xlim([0, 1])
plt.tight_layout()
plt.grid()
plt.show()
