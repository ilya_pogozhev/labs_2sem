import matplotlib.pyplot as plt
import numpy as np


def quantize(array, q_step):
    return [round(ele) // q_step * q_step for ele in array]


# аналоговый сигнал
time = np.arange(0, 2, 0.001)   # шаг по времени = 0.001
function = 2 * np.cos(2 * np.pi * 5 * time) + np.cos(2 * np.pi * time) + np.sin(2 * np.pi * 10 * time)

# квантование
q_func = quantize(function, 1)

# значение ошибок квантования
q_err = function - q_func

# максимальная ошибка квантования
max_q_err = max(abs(q_err))
print(max_q_err)

fig, ax = plt.subplots(2, 1, figsize=(13, 10))

index = 1
for a_x in ax.flat:
    if index == 1:
        a_x.set(title="Сигнал", xlabel='Время, с', ylabel='Амплитуда')
    else:
        a_x.set(title="Ошибки квантования", xlabel='Время, с', ylabel='Амплитуда')

    a_x.grid()

    index += 1

# График
ax[0].plot(time, function)
ax[0].step(time, q_func)
ax[1].plot(time, q_err)

plt.show()