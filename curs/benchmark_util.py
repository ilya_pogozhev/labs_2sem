import random
import matplotlib.pyplot as plt
from numpy import poly1d as np_poly1d, polyfit as np_polyfit
from lab1lib import counter

def create_lst(length, sort=False, reverse=False):
    """ Create a list of tuples with random values.

    :param length: list length
    :param sort: flag, value False, which means that there is no need to sort the created list and vice versa
    :param reverse: flag, value False, which means that in case of sorting
            the list will go in the usual order, True - in reverse order
    :return: list of tuples with random values.
    """

    lst = [(i, random.randint(1, 10)) for i in range(1, length + 1)]
    if sort:
        lst.sort(key=lambda x: x[1], reverse=reverse)
    return lst


def correct_lines_c(f):
    """Selecting the required data about function calls from the file.

    :param f: file with data on function calls
    :return: number of function calls
    """

    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '' and line.split()[0].isnumeric() and not line.count('function')]
    for line in lines_1:
        if line.find('counter') != -1:
            result = line.split('   ')[0]
            break
    return result


def correct_lines_m(f):
    """Selecting the required information about the memory function being used.

    :param f: file with data on function memory
    :return: the amount of memory used within the function.
    """

    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '']
    line = lines_1[len(lines_1)-1].split('   ')[1]
    result = line.strip().split(' ')[0]
    return float(result)


def get_graph(x_lst, y_lst, deg, leg,
              line_1='-r', line_2='--b'):
    """ Function graph display.

    :param line_2:
    :param line_1:
    :param leg:
    :param deg:
    :param x_lst: Ox axis values
    :param y_lst:  Oy axis values for the function
    :return: None
    """

    trend = np_poly1d(np_polyfit(x_lst, y_lst, deg))
    plt.plot(x_lst, trend(x_lst), line_1)
    plt.plot(x_lst, y_lst, line_2, label=leg)
    plt.legend()
    return trend




