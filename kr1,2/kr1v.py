import math
import kr1v_lib
# 0,1  0,1
# 0,1 0,2 worked
# g интервал от 4 до 8
a = 0.1
x = 0.2


def gi(x):
    return (5 * (10 * a ** 2 - 11 * a * x + x ** 2)) / (24 * a ** 2 - 49 * a * x + 15 * x ** 2)


print('left rectangle', kr1v_lib.left_rectangle_rule(gi, 4, 10, 5))
print('right rectangle', kr1v_lib.right_rectangle_rule(gi, 4, 10, 5))
print('mid rectangle', kr1v_lib.midpoint_rectangle_rule(gi, 4, 10, 5))
print('trapezoid', kr1v_lib.trapezoid(gi, 4, 10, 5))
print('simpson', kr1v_lib.simpson(gi, 4, 10, 5))

