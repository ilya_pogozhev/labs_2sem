import matplotlib.pyplot as plt
import kr1v_lib

a = 0.1
x = 0.2

integrated_values = ([], [], [], [], [])
inter = []


def gi(x):
    return (5 * (10 * a ** 2 - 11 * a * x + x ** 2)) / (24 * a ** 2 - 49 * a * x + 15 * x ** 2)


for i in range(10, 100, 10):
    integrated_values[0].append(kr1v_lib.left_rectangle_rule(gi, 4, 10, i))
    integrated_values[1].append(kr1v_lib.right_rectangle_rule(gi, 4, 10, i))
    integrated_values[2].append(kr1v_lib.midpoint_rectangle_rule(gi, 4, 10, i))
    integrated_values[3].append(kr1v_lib.trapezoid(gi, 4, 10, i))
    integrated_values[4].append(kr1v_lib.simpson(gi, 4, 10, i))
    inter.append(i)

for i in range(5):
    for j in range(len(integrated_values[i])):
        integrated_values[i][j] = (-1.9773 - integrated_values[i][j])

fig, ax = plt.subplots()

ax.plot(inter, integrated_values[0], label="LeftRectanglesRule")
ax.plot(inter, integrated_values[1], color="#17becf", label="RightRectanglesRule")
ax.plot(inter, integrated_values[2], color='b', label="MiddleRectanglesRule")
ax.plot(inter, integrated_values[3], color='y', label="TrapeziumRule")
ax.plot(inter, integrated_values[4], color='m', label="SimpsonRule")

ax.set(title="difference between the true and calculated value",
       xlabel="Number of split points", ylabel="Absolute accuracy")
ax.grid(), ax.legend()
plt.show()
