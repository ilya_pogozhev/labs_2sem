from random import randint
from string import ascii_uppercase
from random import choice
from numpy import poly1d, polyfit
from matplotlib import pyplot as plt


def create_array(length):
    array_int = sorted([randint(1, 100) for i in range(1, length + 1)])
    search_str = ''.join(choice(ascii_uppercase[0:4]) for i in range(length + 1))
    return array_int, search_str


def calls_counter(file):
    result = 1
    lines = [i.strip() for i in file]
    lines_1 = [i for i in lines if i not in '' and i.split()[0].isnumeric() and not i.count('function')]
    for line in lines_1:
        if line.find('counter') != -1:
            result = line.split('   ')[0]
            break
    return result


def graph_creater(x_lst, y_lst, deg, ylabel):
    trend = poly1d(polyfit(x_lst, y_lst, deg))
    plt.plot(x_lst, trend(x_lst), '-og', label='Теоретическая сложность')
    plt.xlabel('Размер массива')
    plt.ylabel(ylabel)
    plt.plot(x_lst, y_lst, '--b', label='Практическая сложность')
    plt.xlabel('Размер массива')
    plt.tight_layout()
    plt.legend()
    plt.show()
