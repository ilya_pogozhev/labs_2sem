import timeit
import lab2v_lib
from random import randint

start = timeit.default_timer()
print(lab2v_lib.LinearSearch([1, 2, 3, 4, 5, 2, 1, 6, 9, 7], 5))
stop = timeit.default_timer()
print(f'time Linear = {stop-start}')
print(lab2v_lib.BinarySearch([10, 20, 30, 40, 50, 60, 70, 80, 90, 100], 20))
stop2 = timeit.default_timer()

print(f'time Binary = {stop2-stop}')

start = timeit.default_timer()
print(lab2v_lib.NaiveSearch("f pg df we rqtqtesd ", "d"))
stop = timeit.default_timer()

print(f'time naive = {stop-start}')

print(lab2v_lib.kmp("fgdsdg  fsdg fsdfqq v ", "v"))
