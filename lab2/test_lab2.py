import lab2v_lib


def test_linear_search():
    result = lab2v_lib.linear_search([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3], 4)
    assert result == 0


def test_binary_search():
    result = lab2v_lib.binary_search([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3], 5)
    assert result == 4


def test_naive_search():
    result = lab2v_lib.naive_search("f pg df we rqtqtesd ", "p")
    assert result == [2]


def test_kmp():
    result = lab2v_lib.kmp("fgdsdgs  fsdg fsdfqq v ", "s")
    assert result == [3, 6, 10, 15]


def test_linear_search_input():
    result = lab2v_lib.naive_search("fgdsdgs  fsdg fsdfqq v ", "s")
    assert result == [3, 6, 10, 15]




