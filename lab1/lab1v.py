import lab1lib
list_of_nums = []

try:
    list_of_nums = list(map(int, input().split()))
except ValueError:
    print('Неверное значение')
except:
    print('Неизвестная ошибка')


result_bubble = lab1lib.bubble_sort(list_of_nums)
print('Результат пузырьковой сортировки', result_bubble)

result_insertion = lab1lib.insertion_sort(list_of_nums)
print('Результат сортировки вставками', result_insertion)

result_shell = lab1lib.shell(list_of_nums)
print('Результат сортировки Шелла', result_shell)

result_quick = lab1lib.quick_sort(list_of_nums)
print('Результат быстрой сортировки', result_quick)

