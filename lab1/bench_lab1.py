import lab1lib
import timeit
from random import randint
import matplotlib.pyplot as plt

list_of_nums = []
all_time = ([], [], [], [])
start1 = 100
end = 10000
n = 100

for n in range(n):
    list_of_nums.append(int(randint(0, 10)))

fig, a = plt.subplots()

for i in range(start1, end, int((end-start1)/n)):
    for n in range(10):
        list_of_nums.append(int(randint(0, 10)))
    print(f'Iteration {i}')

    start = timeit.default_timer()
    result_bubble = lab1lib.bubble_sort(list_of_nums)
    all_time[0].append(timeit.default_timer() - start)

    start = timeit.default_timer()
    result_insertion = lab1lib.insertion_sort(list_of_nums)
    all_time[1].append(timeit.default_timer() - start)

    start = timeit.default_timer()
    result_shell = lab1lib.shell(list_of_nums)
    all_time[2].append(timeit.default_timer() - start)

    start = timeit.default_timer()
    result_quick = lab1lib.quick_sort(list_of_nums)
    all_time[3].append(timeit.default_timer() - start)


a.plot([num for num in range(len(all_time[0]))], all_time[0], '-m')
a.plot([num for num in range(len(all_time[1]))], all_time[1], '-r')
a.plot([num for num in range(len(all_time[2]))], all_time[2], '-b')
a.plot([num for num in range(len(all_time[3]))], all_time[3], '-g')

plt.title('Computational complexity')
plt.xlabel('Numbers ->')
plt.ylabel('Time ->')

plt.show()


plt.show()




