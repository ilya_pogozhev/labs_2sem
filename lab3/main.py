import networkx as nx
import lib_graph

graph_data = lib_graph.graph_loader('data.txt')
print(graph_data)

nx_graph = lib_graph.get_nx_graph(graph_data)
print(list(nx_graph.nodes))
print(list(nx_graph.edges))

print(nx.shortest_path(nx_graph, source='A', target='M'))