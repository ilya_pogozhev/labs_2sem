import cProfile
import pstats
from lib_graph import get_my_graph, find_all_paths
from lib_graph import calls_counter
import networkx as nx
from matplotlib import pyplot as plt

first = 1
last = 10
step = 1


def graph_creater(x_lst, y_lst):
    plt.plot(x_lst, y_lst, '-ob', label='Эксперем.')
    plt.title('Практическая сложность графа')
    plt.ylabel('Количество операций')
    plt.xlabel('Размер массива')
    plt.tight_layout()
    plt.legend()
    plt.show()


len_arr = [i for i in range(first, last, step)]
operations = []

for i in range(first, last, step):
    G = nx.complete_graph(i)
    graph = get_my_graph(list(G.nodes()), list(G.edges()))
    cProfile.run('find_all_paths(graph, 0, i)', 'stats.log')
    with open('output.txt', 'w') as log_file:
        p = pstats.Stats('stats.log', stream=log_file)
        p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = calls_counter(f)
    f.close()
    operations.append(int(line))
    print(i)
graph_creater(len_arr, operations)
operations = []
